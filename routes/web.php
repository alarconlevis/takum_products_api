<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Categories;

Route::get('', function(){
    return view('welcome');
});

/*Route::get('create', function () {
    $category = new Categories();
    $category->save();

    foreach (['en', 'fr'] as $locale) {
        $category->translateOrNew($locale)->name = "Name {$locale}";
    }
    $category->save();

    echo 'Created an article with some translations!';
});



Route::get('{locale}', function($locale) {
    app()->setLocale($locale);
    $category = Categories::first();

    return view('test')->with(compact('category'));
});
*/