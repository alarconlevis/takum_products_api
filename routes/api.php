<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '{locale}'], function () {

    /**
     * To create a new category
     */
    Route::get('/categories', [
        'uses' => 'CategoriesController@getCategories'
    ]);


    /**
     * To get all products
     */
    Route::get('/products', [
        'uses' => 'ProductController@getProducts'
    ]);

});

/**
 * To update a product
 */
Route::put('/product/{id}', [
    'uses' => 'ProductController@putProduct'
]);

/**
 * To delete one specific product
 */
Route::delete('/delete/{id}', [
    'uses' => 'ProductController@deleteProduct'
]);


/**
 * To create a new product
 */
Route::post('/product', [
    'uses' => 'ProductController@postProduct'
]);

/**
 * To create a new category
 */
Route::post('/category', [
    'uses' => 'CategoriesController@postCategory'
]);