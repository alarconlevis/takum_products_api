<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['name', 'desc'];
    public $fillable = ['price'];
}
