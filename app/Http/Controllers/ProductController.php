<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function postProduct(Request $request){
        $product = new Product();
        $product->price = $request->input('price');
        $product->category_id = $request->input('category');
        $product->save();
        $product->translateOrNew('en')->name = $request->input('enName');
        $product->translateOrNew('fr')->name = $request->input('frName');
        $product->translateOrNew('en')->desc = $request->input('enDesc');
        $product->translateOrNew('fr')->desc = $request->input('frDesc');
        $product->save();
        return response()->json(['product' => $product], 201);
    }

    public function getProducts($locale){
        app()->setLocale($locale);
        $products = Product::all();
        return response()->json(['products' => $products], 200);
    }

    public function putProduct(Request $request, $id){
        $product = Product::find($id);
        if(!$product){
            return response()->json(['message' => 'Product doesnt exist'], 404);
        }
        $product->price = $request->input('price');
        $product->category_id = $request->input('category');
        $product->save();

        $product->translateOrNew('en')->name = $request->input('enName');
        $product->translateOrNew('fr')->name = $request->input('frName');
        $product->translateOrNew('en')->desc = $request->input('enDesc');
        $product->translateOrNew('fr')->desc = $request->input('frDesc');
        $product->save();

        return response()->json(['product' => $product], 200);
    }

    public function deleteTask($id){
        $product = Product::find($id);
        $product->delete();

        return response()->json(['message' => 'Product deleted'], 200);
    }
}
