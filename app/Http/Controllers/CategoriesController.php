<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function postCategory(Request $request){
        $category = new Category();
        $category->save();
        $category->translateOrNew('en')->name = $request->input('enName');
        $category->translateOrNew('fr')->name = $request->input('frName');
        $category->save();
        return response()->json(['category' => $category], 201);
    }

    public function getCategories($locale){
        app()->setLocale($locale);
        $categories = Category::all();
        return response()->json(['categories' => $categories], 200);
    }
}
